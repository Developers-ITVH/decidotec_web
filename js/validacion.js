'use strict'
var formulario = document.querySelector("#formulario");

formulario.addEventListener('submit', ()=>{

var usuario = document.querySelector("#camp-user").value;
var pass = document.querySelector("#camp-pass").value;

if (usuario.trim() == null || usuario.trim().length == 0) {
	document.querySelector("#error_user").innerHTML = "El usuario no es valido";
	return false;
}else{
	document.querySelector("#error_user").style.display = "none";
}

if (pass.trim() == null || pass.trim().length == 0) {
	document.querySelector("#error_pass").innerHTML = "La contraseña no es valido";
	return false;
}else{
	document.querySelector("#error_pass").style.display = "none";
}

});
