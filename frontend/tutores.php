<?php 
$usuarios = json_decode(file_get_contents("https://www.bringsolutions.com.mx/decidotec/api.php?accion=consultaTutor"), true);
$array = $usuarios["decidotec"];


 ?>

  <table class="table table-hover table-fixed table-responsive"  style=" height:87vh; overflow: scroll;">
 
      <thead>
        <tr>
            <th>#</th>

          <th>Nombre</th>
          <th>Apellido paterno</th>
          <th>Apellido materno</th>
          <th>Teléfono</th>
          <th>Email</th>
          <th>Editar</th>
          <th>Eliminar</th>
        </tr>
      </thead>

      <tbody id="#">
      	<?php 
      	foreach ($array as $key => $value) {
          $id = $value["ID_TUTOR"];
	$nombre = $value['NOMBRE'];
	$apellidoM = $value['APELLIDO_PATERNO'];
	$apellidoP = $value['APELLIDO_MATERNO'];
  $telefono = $value['TELEFONO'];
  $email = $value['EMAIL'];
      	 ?>

          <tr>
           <th scope="row"><?php echo ($key); ?></th>
           <td><?php echo "$nombre"; ?></td>
           <td><?php echo "$apellidoP"; ?></td>
           <td><?php echo "$apellidoM"; ?>"></td>
           <td><?php echo "$telefono"; ?></td>
           <td><?php echo "$email"; ?></td>
           <td><a href="#datos?id=<?php echo $id?>" class="btn btn-success btn-sm" data-toggle="modal" data-target="#basicExampleModal"><i class="fas fa-pencil-alt"></i></a> </td>
           <td><a href="#?id=<?php echo $id?>" class="btn btn-danger btn-sm"><i class="fas fa-trash-alt"></i></a></td>
          </tr>
          <div class="modal fade" id="basicExampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p id="datos"><?php echo "$id"; ?></p>
        <p></p>
        <p></p>
        <p></p>
        <p></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-success">Save changes</button>
      </div>
    </div>
  </div>
</div>
              <?php }; ?>
      </tbody>
    </table>



<!-- Modal -->
