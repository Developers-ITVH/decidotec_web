<div class="container padding-foto">

  <form action="https://www.bringsolutions.com.mx/decidotec/apifile.php?" method="POST" enctype="multipart/form-data">
    <input type="text"  name ="accion" value="registrarNotificacion" style="visibility: hidden;">
    <div class="row">

        <div class="col-6">
         <div class="cont-input m-auto">
            <div class="foto" id="preview">
             <img src="img/upload.png" class="imagen-notificacion" >
           </div>
           
                 <center>
                   <label class="archivolabel" for="cambiar">Subir foto</label><br>
                   <input type="file" class="archivoinput" id="cambiar" name="img" required="">
                 </center>
                 
         </div>
       </div>
           
 
        <div class="col-6">
        
          <label for="exampleForm2">Titulo</label>
          <input type="text" id="exampleForm2" class="form-control" name="titulo" required="">
    
          <label for="exampleForm2">Link</label>
          <input type="text" id="exampleForm2" class="form-control" name="link" required="">
    
          <label for="exampleForm2">descripción</label>
          <textarea class="form-control rounded-0" id="exampleFormControlTextarea2" rows="3" name="descripcion" required=""></textarea><br>
    
          <button type="submit" class="btn btn-success">Enviar</button>
        </div>
    
        </div>
  </form>
  </div>
<script type="text/javascript">
    (function(){
    function filePreview(input){
    if(input.files && input.files[0]){
    var reader = new FileReader();
    reader.onload = function(e){
    $('#preview').html("<img class='imagen-notificacion' src='"+e.target.result+"' />");
      }
    reader.readAsDataURL(input.files[0]);
      }
      }
    $('#cambiar').change(function(){
    filePreview(this);
      });

      })();
</script>