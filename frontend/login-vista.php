<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Login</title>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="css/mdb.min.css" rel="stylesheet">
  <!-- Your custom styles (optional) -->
  <link href="css/login.css" rel="stylesheet">

</head>

<body>

<!-- Default form login -->
<div class="container cont-login">
  <div class="row">
  <div class="col-sm-12 col-md-8 col-lg-8 col-xl-8 m-auto">
<form class="text-center border border-adnger p-5" method="post" id="formulario" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>"> 
    
        <p class="h4 mb-4">#Decido Tec</p>
        
        <div class="md-form input-group mb-3">
          <i class="fas fa-user"></i>
          <input type="text"  name="user" class="form-control" placeholder="Usuario" aria-label="Username" aria-describedby="material-addon1" id="camp-user" required=""><br>
        </div>

         <div class="md-form input-group mb-3">
          <span class="text-danger" id="error_user"></span>
        </div>
    
        <div class="md-form input-group mb-3">
          <i class="fas fa-unlock-alt"></i>
          <input type="password" name="pass" class="form-control" placeholder="contraseña" aria-label="Username" aria-describedby="material-addon1"id="camp-pass" required=""><br>
        </div>

         <div class="md-form input-group mb-3">
          <span class="text-danger" id="error_pass"></span>
        </div>
        
        <div>
            <!-- Forgot password -->
            <a href="" data-toggle="modal" data-target="#modalLoginForm">Olvidaste tu contraseña?</a>
        </div>
    
        <!-- Sign in button -->
        <button class="btn  btn-block my-4 success-color" type="submit">Acceder</button>
    
    </form>
  </div>
  </div>

</div>
<!-- Default form login -->


<div class="modal fade" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <form action="recuperar.php" method="post">
      <div class="modal-content">
      <div class="modal-header text-center">
        <h4 class="modal-title w-100 font-weight-bold">Recuperar contraseña</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" >
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body mx-3">
        <div class="md-form mb-5">
          <p class="text-center">Ingresa tu correo para recuperar tu contraseña</p>
        </div>

        <div class="md-form mb-4"> 
          <i class="fas fa-user-alt prefix grey-text"></i>

          <input type="text" id="defaultForm-email" class="form-control validate" name="usuario" required="">
          <label data-error="wrong" data-success="OK" for="defaultForm-email">Usuario</label>
        </div>

        <div class="md-form mb-4"> 
          <i class="fas fa-envelope prefix grey-text"></i>
          <input type="email" id="defaultForm-email" class="form-control validate" name="recuperar" required="">
          <label data-error="wrong" data-success="OK" for="defaultForm-email">Email</label>
        </div>

      </div>
      <div class="modal-footer d-flex justify-content-center">
        <button class="btn   success-color" type="submit">Recuperar</button>
      </div>
    </div>
    </form>
  </div>
</div>



  <!-- SCRIPTS -->
  <script type="text/javascript" src="js/validacion.js"></script>
  <!-- JQuery -->
  <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="js/popper.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="js/bootstrap.min.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="js/mdb.min.js"></script>
</body>

</html>
