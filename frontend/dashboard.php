<?php
include_once'conexion.php';

$usuario = $_SESSION['usuario'];                  
foreach ($conn->query("SELECT * FROM administradores WHERE usuario = '$usuario'  ") as $extraer){ }
  ?>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>DecidoTec</title>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="css/mdb.min.css" rel="stylesheet">
  <!-- Your custom styles (optional) -->
  <link href="css/style.css" rel="stylesheet">
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

</head>

<body>
<!--Navbar -->

<nav class="mb-1 navbar navbar-expand-lg navbar-dark  lighten-1 success-color fixed-top">
  <a class="navbar-brand" href="#"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-555"
    aria-controls="navbarSupportedContent-555" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent-555">
    <ul class="navbar-nav ml-auto nav-flex-icons">
      <li class="nav-item">

        <a class="nav-link waves-effect waves-light">
          <?php echo $extraer["nombre"] ?>
        </a>
         
      </li>
      <li class="nav-item avatar dropdown">
        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-55" data-toggle="dropdown" aria-haspopup="true"
          aria-expanded="false">
          <img src="img/user.jpg" class="rounded-circle z-depth-0" alt="avatar image" height="30">
        </a>
        <div class="dropdown-menu dropdown-menu-right dropdown-secondary" aria-labelledby="navbarDropdownMenuLink-55">
          <a class="dropdown-item" href="cerrar.php"><i class="fas fa-file-export"></i> Cerrar sesión</a>
        </div>
      </li>
    </ul>
  </div>
</nav>
<!--/.Navbar -->
<div class="cont-dash">
  <div class="sidebar">
    <a href="#" id="prospectos" class="active"><i class="fas fa-graduation-cap"></i> Prospecto</a>
    <a href="#" id="tutores"><i class="fas fa-users"></i> Tutores</a>
    <a href="#" id="preparatoria"><i class="fas fa-book-open"></i> Preparatorias</a>
    <a href="#" id="agregar"><i class="fas fa-plus"></i> preparatorias</a>
    <a href="#" id="notificaciones"><i class="far fa-bell"></i> Notificación</a>
    <a href="#" id="club"><i class="fab fa-cc-diners-club"></i> Club</a>
    <a href="#" id="estadisticas"><i class="fas fa-chart-line"></i> Estadísticas</a>

  </div>
  
  <div class="content" id="contenido" >
    <span class="cargando">Cargando...</span><br><br>
    <center>
      <h1 class="h1"><span class="hola">¡Hola!</span> <?php echo $extraer["nombre"]; ?> </h1>
      <!--<div class="m-auto"><img src="img/unnamed.png" class="m-auto"></div>-->
    </center>
  </div>
    </div>


  <!-- JQuery -->
  <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
  <!-- SCRIPTS -->
  <script type="text/javascript" src="js/peticiones.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="js/popper.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="js/bootstrap.min.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="js/mdb.min.js"></script>
</body>

</html>
