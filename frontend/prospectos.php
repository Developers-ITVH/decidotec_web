   <?php 
   error_reporting(0);
$datos = json_decode(file_get_contents("https://www.bringsolutions.com.mx/decidotec/api.php?accion=consultarProspectos"),true);
$array = $datos["decidotec"];

 ?>
   <table class="table table-hover table-fixed table-responsive " style=" height:87vh; overflow: scroll;">
  <thead>
    <tr>
      <th>#</th>
      <th>NOMBRE</th>
      <th>APELLIDO PATERNO</th>
      <th>APELLIDO MATERNO</th>
      <th>TELEFONO</th>
      <th>E-MAIL</th>
      <th>CARRERA</th>
      <th>EDITAR</th>
      <th>ELIMINAR</th>

    </tr>
  </thead>
  <!--Table head-->

  <!--Table body-->
  <tbody>
    <?php 
  foreach ($array as $value) {
  $id = $value["ID_PROSPECTO"];
  $nombre = $value["NOMBRE"];
  $apellidoP = $value["APELLIDO_PATERNO"];
  $apellidoM =$value["APELLIDO_MATERNO"];
  $telefono = $value["TELEFONO"];
  $email = $value["EMAIL"];
  $carrera = $value["CARRERA"];
  $contacto = $value["MEDIO_CONTACTO"];
                ?>
  <tr>
      <th scope="row"><?php echo "$id"; ?></th>
      <td><?php echo "$nombre";  ?></td>
      <td><?php echo "$apellidoP";  ?></td>
      <td><?php echo "$apellidoM";  ?></td>
      <td><?php echo "$telefono";  ?></td>
      <td><?php echo "$email";  ?></td>
      <td><?php echo "$carrera";  ?></td>
      <td><a href="#?id=<?php echo $id?>" class="btn btn-success btn-sm"><i class="fas fa-pencil-alt"></i></a> </td>
      <td><a href="#?id=<?php echo $id?>" class="btn btn-danger btn-sm"><i class="fas fa-trash-alt"></i></a></td>
    </tr>
    <?php }; ?>
  </tbody>
 </table>