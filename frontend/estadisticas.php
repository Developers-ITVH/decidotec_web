<?php

    $datos=json_decode(file_get_contents("https://www.bringsolutions.com.mx/decidotec/api.php?accion=totalCarreras"),true);
    $array = $datos["decidotec"];

    $datos_inscritos=json_decode(file_get_contents("https://www.bringsolutions.com.mx/decidotec/api.php?accion=totalInscrito"),true);
    $inscrito = $datos_inscritos["decidotec"];
?>


    <script type="text/javascript">
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(crearGrafica);

        function crearGrafica(){
            var data = new google.visualization.DataTable();

                data.addColumn('string','ciudad');
                data.addColumn('number','visitas');
                data.addRows([

                <?php 
                    foreach ($array as $valor){
                        $nombre = $valor["NOMBRE"];
                        $prepartoria = $valor["NUM_PROSPECTOS"];
                        echo("['".$nombre."',".$prepartoria."],");
                
                    };
                    
                ?>
            ]);

            var opciones={'title':'Preparatorias','width':1000,'height':500};
            var grafica=new google.visualization.PieChart(document.getElementById('grafica1'));
            grafica.draw(data,opciones);
        }

        
    </script>

    <script type="text/javascript">
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(crearGrafica);

        function crearGrafica(){
            var data = new google.visualization.DataTable();

                data.addColumn('string','ciudad');
                data.addColumn('number','visitas');
                data.addRows([

                <?php 
                    foreach ($inscrito as $valor){
                        $nombre = $valor["CARRERA"];
                        $prepartoria = $valor["NUM_CARRERAS"];
                        echo("['".$nombre."',".$prepartoria."],");
                
                    };
                    
                ?>
            ]);

            var opciones={'title':'Aspirantes a carreras','width':1000,'height':500};
            var grafica=new google.visualization.PieChart(document.getElementById('grafica2'));
            grafica.draw(data,opciones);
        }

        
    </script>
  

        


         


 
               


<table class="table  table-fixed table-responsive"  style=" height:87vh; overflow: scroll;">
  <tbody id="#">
    <tr>
      <td><div id="grafica1">

        </div></td>
      <td ><div id="grafica2" >

        </div></td>
    </tr>
  </tbody>
</table>




   
